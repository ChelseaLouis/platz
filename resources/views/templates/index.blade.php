<!DOCTYPE HTML>
<html>

  <head>
    @include('templates.partials._head')
  </head>

  <body>

      @include('templates.partials._ancre')

      @include('templates.partials._cache')

      <!-- APP -->
      <div id="app">

          <app-header></app-header>

          <menu-categories></menu-categories>


          @include('templates.partials._portfolio')

      </div>

      @include('templates.partials._scripts')

   </body>

</html>
