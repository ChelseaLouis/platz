<!-- PORTFOLIO -->

<div id="wrapper-container">
  <div class="container object">
    <div id="main-container-image">
        <div id="app">

          <router-view></router-view>

        </div>
    </div>
  </div>


  @include('templates.partials._thank')

  @include('templates.partials._footer')

  @include('templates.partials._copyright')

</div>
