
let actions = {

  /**
   * [setPosts description]
   * @param {[type]} commit [description]
   */
  setPosts ({commit}) {
    // Transaction AJAX
    axios.get('api/posts')
         .then(reponsePHP => (commit('SET_POSTS', reponsePHP.data)));
  },

  /**
   * [setCategories description]
   * @param {[type]} commit [description]
   */
  setCategories ({commit}) {
    // Transaction AJAX
    axios.get('api/categories')
         .then(reponsePHP => (commit('SET_CATEGORIES', reponsePHP.data)));
  },

  /**
   * [setComments description]
   * @param {[type]} commit [description]
   */
  setComments ({commit}) {
    // Transaction AJAX
    axios.get('api/comments')
         .then(reponsePHP => (commit('SET_COMMENTS', reponsePHP.data)));
  },

  /**
   * [createComment description]
   * @param  {[type]} commit  [description]
   * @param  {[type]} comment [description]
   * @return {[type]}         [description]
   */
   createComment({commit}, comment) {
     // https://laravel.com/docs/8.x/controllers#basic-controllers
    axios.post('api/comments', comment)
        .then(reponsePHP => (commit('CREATE_COMMENT', reponsePHP.data)))
        .catch(err => {
             console.log(err)
        })
    axios.get('api/comments')
    .then(reponsePHP => (commit('SET_COMMENTS', reponsePHP.data)));
   }

};

export default actions;
