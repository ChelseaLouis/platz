
let getters = {

  /**
   * ALL POSTS
   * @param  {[type]} state [description]
   * @return {[type]}       [description]
   */
    getPosts (state) {
      return state.posts;
    },

  /**
   * POST BY ID
   * @param  {[type]} state [description]
   * @return {[type]}       [description]
   */
    getPostById (state) {
      return function (id) {
        return state.posts.find(post => post.id == id);
      }
    },

  /**
   * POSTS BY CATEGORIEID
   * @param  {[type]} state [description]
   * @return {[type]}       [description]
   */
    getPostsByCategorieId (state){
      return function (id) {
        return state.posts.filter(posts => posts.categorie_id == id);
      }
    },

  /**
   * ALL CATEGORIES
   * @param  {[type]} state [description]
   * @return {[type]}       [description]
   */
    getCategories (state) {
      return state.categories;
    },


  /**
   * COMMENTS BY POSTID
   * @param  {[type]} state [description]
   * @return {[type]}       [description]
   */
    getCommentsByPostId (state) {
      return function (id) {
        return state.comments.filter(comments => comments.post_id == id);
      }
    },

    getUsers (state) {
      return state.users;
    },

};

export default getters
