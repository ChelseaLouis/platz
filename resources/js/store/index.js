// ./src/js/store/index.js
import Vue  from 'vue'
import Vuex from 'vuex'

import moment from 'moment'

Vue.filter('dateFormat', (arg) => {
    return moment(arg).format('ll')
});

import state from './state.js';
import getters from './getters.js';
import mutations from './mutations.js';
import actions from './actions.js';

Vue.use(Vuex);

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions
})
