let mutations = {
  SET_POSTS (state, data) {
    state.posts = data;
  },

  SET_CATEGORIES (state, data) {
    state.categories = data;
  },

  SET_COMMENTS (state, data) {
    state.comments = data;
  },

  CREATE_COMMENT (state, comment) {
    state.comments.unshift(comment);
  }
};

export default mutations;
