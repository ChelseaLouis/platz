<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;

class Comments extends Controller
{
  public function index() {
    
      return response()->json(Comment::with('post','user')->get());
  }

  public function store(Request $request){
      $request->validate([
          'content' => 'required',
          'post_id' => 'required',
          'user_id' => 'required',
      ]);

    return Comment::create($request->only(['content', 'post_id', 'user_id']));
  }
}
