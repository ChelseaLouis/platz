<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subcategorie extends Model
{
    use HasFactory;

    /**
     * GETTER des posts de la sous-catégorie.
     * @return [type] [description]
     */
    public function posts() {
      return $this->hasMany('App\Models\Post');
    }
}
