<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    // use HasFactory;

        /**
     * GETTER de la categorie à laquelle appartient ce post.
     * @return [type] [description]
     */
    public function categorie(){
      return $this->belongsTo('App\Models\Categorie');
    }

    /**
     * GETTER de la sous-categorie à laquelle appartient ce post.
     * @return [type] [description]
     */
    public function subcategorie(){
      return $this->belongsTo('App\Models\Subcategorie');
    }

    /**
    * GETTER du user à qui appartient ce post.
    * @return [type] [description]
    */
    public function user(){
      return $this->belongsTo('App\Models\User');
    }

}
