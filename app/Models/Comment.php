<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;
    protected $table = 'comments';
    protected $fillable = ['content', 'post_id', 'user_id'];

    /**
     * GETTER du post auquel appartient ce commentaire.
     */
    public function post(){
        return $this->belongsTo('App\Models\Post');
    }

    /**
     * GETTER du user à qui appartient ce commentaire.
     */
    public function user(){
        return $this->belongsTo('App\Models\User');
    }

}
